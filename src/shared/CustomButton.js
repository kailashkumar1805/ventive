import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import AppStyles from '../config/styles';

const CustomButton = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity 
                style={(props.type === 'primary') ? styles.buttonPrimaryStyle : styles.buttonSecondaryStyle}
                onPress={() => props.onPressListner()}
            >
                <Text style={styles.textStyle}>{props.children}</Text>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10,
    },
    buttonPrimaryStyle: {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        backgroundColor: AppStyles.color.COLOR_PRIMARY_BUTTON,
        borderRadius: 25,
        padding: 16,
        alignItems: 'center'
    },
    buttonSecondaryStyle: {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        backgroundColor: AppStyles.color.COLOR_SENCONDARY_BUTTON,
        borderRadius: 25,
        alignItems: 'center',
        padding: 16,
    },
    textStyle: {
        color: '#ffffff',
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        fontWeight: 'bold',
        fontSize: 16
    }
})

export default CustomButton;
