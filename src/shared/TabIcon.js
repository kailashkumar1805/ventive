import React from 'react';
import { View, Image } from 'react-native';
import AppStyles from '../config/styles';

const TabIcon = (props) => {
    return (
        <View style={props.params.focused ? AppStyles.selectTabIcon : ''}>
            <Image
                style={[AppStyles.bottomIconStyle, {tintColor: props.params.focused ? AppStyles.color.COLOR_PRIMARY_BUTTON : AppStyles.color.COLOR_SENCONDARY_BUTTON}]}
                source={props.img}
            />
        </View>
    )
}

export default TabIcon;
