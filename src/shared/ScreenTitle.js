import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import AppStyles from '../config/styles';

const ScreenTitle = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.textStyle}>{props.title}</Text>
        </View>
    )
}

export default ScreenTitle;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flex: 1
    },
    textStyle : {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        color: AppStyles.color.COLOR_SENCONDARY_BUTTON,
        fontSize: 22,
        fontWeight: "bold"
    }
});
