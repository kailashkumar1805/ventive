import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image, ScrollView, KeyboardAvoidingView } from 'react-native';
import { selectors } from '@selectors';
import { actions } from '@actions';
import { validateEmail } from '../utils/helper';
import AppStyles from '../config/styles';
import Toast, { DURATION } from 'react-native-easy-toast';
import CustomButton from '../shared/CustomButton';



class SignUpScreen extends Component {

    constructor(props) {
        super(props);

        this.inputs = {};
        this.toast = React.createRef();

        this.state = {
            email: '',
            password: '',
            mobile: '',
            fullname: '',
            submit: false,
            error: {

            }
        };

        this.signUpScreen = this.signUpScreen.bind(this);
        this.onPressListner = this.onPressListner.bind(this);
        this.onChangeListener = this.onChangeListener.bind(this);
        this.focusNextField = this.focusNextField.bind(this);
        this.onBlurListener = this.onBlurListener.bind(this);

    }

    onChangeListener(event, type) {
        const value = event.nativeEvent.text;
        const error = this.state.error;
        switch (type) {
            case 'email':
                if (!validateEmail(value)) {
                    error.email = "Please enter valid email address!";
                } else {
                    error.email = undefined;
                }
                this.setState({ email:value, error });
                break;
            case 'password':
                if (value != '') {
                    this.setState({ password: value })
                }
                break;
            case 'fullname':
                if (value != '') {
                    this.setState({ fullname: value })
                }
                break;
        }

    }

    /**
     * 
     * @param {*} id 
     */
    onBlurListener(type, fieldType) {
        if (this.state.error[fieldType]) {
            this.refs.toast.show(this.state.error[fieldType], 3000);
        }
        this.focusNextField(type);
    }


    focusNextField(id) {
        this.inputs[id].focus();
    }

    signUpScreen() {
        const { navigation } = this.props;
        navigation.navigate('SignIn');
    }

    async onPressListner() {
        const { navigation } = this.props;
        this.setState({ submit: true });
        const { error } = this.state;
        if(error.email === undefined && this.state.email !== '' && this.state.password !=='' && this.state.fullname !== '') {
            const payload = {
                email: this.state.email,
                password: this.state.password,
                fullname: this.state.fullname,
            }
    
            await this.props.signUpAccess(payload);
            navigation.navigate('SignIn');
        }
    }


    render() {
        const { error, submit } = this.state;
        const { navigation } = this.props;
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                <ScrollView style={styles.container}>
                    <SafeAreaView style={{ flex: 1 }}>

                        <View style={styles.formStyle}>
                            <TextInput
                                placeholderTextColor={AppStyles.color.COLOR_PLACEHOLDER}
                                placeholder="Email"
                                style={[AppStyles.inputStyle, (error.email || this.state.email == '' && submit) ? AppStyles.errorInputStyle : '']}
                                onSubmitEditing={() => {
                                    this.onBlurListener('password', 'email');
                                }}
                                blurOnSubmit={false}
                                returnKeyType={"next"}
                                ref={input => {
                                    this.inputs['email'] = input;
                                }}
                                onChange={(e) => this.onChangeListener(e, 'email')}
                            />
                        </View>
                        <View style={styles.formStyle}>
                            <TextInput
                                placeholderTextColor={AppStyles.color.COLOR_PLACEHOLDER}
                                placeholder="Password"
                                style={[AppStyles.inputStyle, (this.state.password == '' && submit) ? AppStyles.errorInputStyle : '']}
                                secureTextEntry={true}
                                onSubmitEditing={() => {
                                    this.onBlurListener('fullname', 'password');
                                }}
                                blurOnSubmit={false}
                                returnKeyType={"next"}
                                ref={input => {
                                    this.inputs['password'] = input;
                                }}
                                onChange={(e) => this.onChangeListener(e, 'password')}
                            />
                        </View>
                        <View style={styles.formStyle}>
                            <TextInput
                                placeholderTextColor={AppStyles.color.COLOR_PLACEHOLDER}
                                placeholder="Full Name"
                                style={[AppStyles.inputStyle, (this.state.fullname == '' && submit) ? AppStyles.errorInputStyle : '']}
                                onSubmitEditing={() => {
                                    this.onBlurListener('mobile', 'fullname');
                                }}
                                blurOnSubmit={false}
                                returnKeyType={"next"}
                                ref={input => {
                                    this.inputs['fullname'] = input;
                                }}
                                onChange={(e) => this.onChangeListener(e, 'fullname')}
                            />
                        </View>
                        <View style={styles.btnContainer}>
                            <CustomButton onPressListner={this.onPressListner} type="secondary">SIGN UP</CustomButton>
                        </View>
                        <Toast
                            ref="toast"
                            style={AppStyles.toastStyle}
                            textStyle={{ color: 'white', paddingLeft: 20, paddingRight: 20 }}
                        />
                        <View style={styles.linkContainer}>
                            <Text style={styles.linkSubTextStyle}>
                                I have already account
                            </Text>
                            <TouchableOpacity onPress={() => navigation.navigate('SignIn')}><Text style={styles.linkTextStyle}>Sign In</Text></TouchableOpacity>
                        </View>
                    </SafeAreaView>
                  
                </ScrollView>
            </KeyboardAvoidingView>

        )
    }
}

const mapStateToProps = (state) => ({
    loading: selectors.getLoadingStatus(state),
});

const mapDispatchToProps = (dispatch) => ({
    signUpAccess: (payload) => actions.signUpAccess(dispatch, payload),
})

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: '#ffffff',
    },
    formStyle: {
        padding: 10,
        flexDirection: 'row',
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    textStyle: {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        fontSize: 12,
        color: AppStyles.color.COLOR_TEXT,
        fontWeight: 'bold',
        paddingTop: 10,
    },
    linkSubTextStyle: {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        fontSize: 12,
        color: AppStyles.color.COLOR_TEXT,
        fontWeight: '200',
        paddingTop: 10,
    },
    linkTextStyle: {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        fontSize: 12,
        color: AppStyles.color.COLOR_PRIMARY_BUTTON,
        fontWeight: 'bold',
        paddingTop: 10,
    },
    linkContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);