import React, { Component } from 'react';
import { SafeAreaView } from 'react-navigation';
import { StyleSheet, View, Text, Image } from 'react-native';
import AppStyles from '../config/styles';
import TabIcon from '../shared/TabIcon';
import ScreenTitle from '../shared/ScreenTitle';

class HomeScreen extends Component {

    static navigationOptions = {
        tabBarLabel: "Home",
        tabBarIcon: (params) => { 
            return ( <TabIcon
                params={params}
                img={require('@assets/img/home.png')}
            /> )
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {
        return(
            <SafeAreaView style={styles.container}>
                <ScreenTitle
                    title="Home"
                />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start'
    },
    titleStyle: {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        color: AppStyles.color.COLOR_SENCONDARY_BUTTON,
        fontSize: 14,
    },
    headerStyle: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: AppStyles.color.COLOR_BORDER,
        borderBottomWidth: 1
    },
    offerContainer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    offerImgStyle: {
        height: 150,
        width: 150
    }
});

export default HomeScreen;