import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Modal, KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import { selectors } from '@selectors';
import { actions } from '@actions';
import AppStyles from '../config/styles';
import CustomButton from '../shared/CustomButton';
import Toast, { DURATION } from 'react-native-easy-toast';

class SignInScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            submit: false
        }

        this.toast = React.createRef();

        this.inputs = {};

        this.onPressListner = this.onPressListner.bind(this);
        this.focusNextField = this.focusNextField.bind(this);
        this.onBlurListener = this.onBlurListener.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { loginAccess, navigation } = this.props;

        if(loginAccess !== nextProps.loginAccess && this.state.submit) {
            this.refs.toast.show("You have login successfully done!", 3000);
            navigation.replace('Home')
        }
    }


    onPressListner(type) {
        const { navigation } = this.props;
        switch (type) {
            case 'sign-in':
                this.setState({ submit: true });
                if (this.state.email !== '' && this.state.password !== '') {
                    this.props.userLogin({ email: this.state.email, password: this.state.password })
                } else {
                    this.refs.toast.show("Please enter email and password!", 3000);
                }
                break;
            case 'sign-up':
                navigation.navigate('SignUp');
                break;
            default:
                break;
        }
    }


    /**
     * @param {*} id 
     */
    onBlurListener(type, fieldType) {

        this.focusNextField(type);
    }

    focusNextField(id) {
        this.inputs[id].focus();
    }

    onChangeListener(event, type) {
        const value = event.nativeEvent.text;
        switch(type) {
            case 'email':
                this.setState({ email: value });
                break;
            case 'password': 
                this.setState({ password: value });
                break;
        }
    }


    render() {
        const { submit } = this.state;

        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                <ScrollView style={styles.scrollContainer}>
                    <SafeAreaView style={styles.container}>
                        <View style={styles.formStyle}>
                            <TextInput
                                placeholderTextColor={AppStyles.color.COLOR_PLACEHOLDER}
                                placeholder="Email"
                                style={[AppStyles.inputStyle, (this.state.email == '' && submit) ? AppStyles.errorInputStyle : '']}
                                blurOnSubmit={true}
                                onSubmitEditing={() => {
                                    this.onBlurListener('password', 'email');
                                }}
                                returnKeyType={"next"}
                                ref={input => {
                                    this.inputs['email'] = input;
                                }}
                                onChange={(e) => this.onChangeListener(e, 'email')}
                            />
                        </View>
                        <View style={styles.formStyle}>
                            <TextInput
                                placeholderTextColor={AppStyles.color.COLOR_PLACEHOLDER}
                                placeholder="Password"
                                style={[AppStyles.inputStyle, (this.state.password == '' && submit) ? AppStyles.errorInputStyle : '']}
                                secureTextEntry={true}
                                blurOnSubmit={true}
                                returnKeyType={"next"}
                                ref={input => {
                                    this.inputs['password'] = input;
                                }}
                                onChange={(e) => this.onChangeListener(e, 'password')}
                            />
                        </View>
                        <View style={styles.btnContainer}>
                            <CustomButton type="primary" onPressListner={() => this.onPressListner('sign-in')}>SIGN IN</CustomButton>
                            <CustomButton type="secondary" onPressListner={() => this.onPressListner('sign-up')}>SIGN UP</CustomButton>
                        </View>
                        <Toast
                            ref="toast"
                            style={AppStyles.toastStyle}
                            textStyle={{ color: 'white', paddingLeft: 20, paddingRight: 20 }}
                        />
                    </SafeAreaView>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state) => ({
    loading: selectors.getLoadingStatus(state),
    loginAccess: selectors.getLoginAccess(state)
});

const mapDispatchToProps = (dispatch) => ({
    userLogin: (payload) => actions.userLogin(dispatch, payload),
})

const styles = StyleSheet.create({
    scrollContainer: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: '#ffffff',
    },
    container: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center'
    },
    formStyle: {
        padding: 10,
        flexDirection: 'row',
    },
    btnContainer: {
        flexDirection: 'row',
    },
    textStyle: {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        fontSize: 12,
        color: AppStyles.color.COLOR_TEXT,
        fontWeight: 'bold',
        padding: 10
    },
    //  modal style start
    filterModalStyle: {
        justifyContent: 'flex-end',
        flexDirection: 'column'
    },
    modalStyle: {
        flexDirection: 'column',
        flex: 1
    },
    headerTextStyle: {
        color: 'white',
        fontSize: 20,
    },
    modalHeaderStyle: {
        alignItems: 'center',
        backgroundColor: AppStyles.color.COLOR_PRIMARY_BUTTON,
        padding: 16,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    iconStyle: {
        height: 14,
        width: 14
    },
    modalContentStyle: {
        backgroundColor: 'white',
        flex: 1
    },
    languageSupport: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    logo: {
        height: 150,
        width: 150
    },
    closeContainer: {
        position: 'absolute',
        top: 20,
        left: 20
    },
    closeStyles: {
        width: 24,
        height: 24,
        tintColor: AppStyles.color.COLOR_PRIMARY_BUTTON
    },
    freeSpaceContainer: {
        flexDirection: 'row',
        flex: 5,
        justifyContent: 'flex-start'
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);