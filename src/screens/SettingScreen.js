import React, { Component } from 'react';
import { connect } from "react-redux";
import { SafeAreaView } from 'react-navigation';
import { StyleSheet } from 'react-native';
import AppStyles from '../config/styles';
import { selectors } from '@selectors';
import { actions } from '@actions';
import TabIcon from '../shared/TabIcon';
import ScreenTitle from '../shared/ScreenTitle';
import CustomButton from '../shared/CustomButton';
import Toast, { DURATION } from 'react-native-easy-toast';


class SettingScreen extends Component {

    static navigationOptions = {
        tabBarLabel: "Setting",
        tabBarIcon: (params) => { 
            return ( <TabIcon
                params={params}
                img={require('@assets/img/settings.png')}
            /> )
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    constructor(props) {
        super(props);
        this.toast = React.createRef();
        this.onPressListener = this.onPressListener.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { loginAccess, navigation } = this.props;

        if(loginAccess !== nextProps.loginAccess) {
            this.refs.toast.show("You have logged out successfully!", 3000);
            navigation.navigate('SignIn');
        }
    }

    /**
     * on press listener to logout app
     */
    onPressListener() {
        this.props.logoutAction();
    }

    render() {
        return(
            <SafeAreaView style={styles.container}>
                <ScreenTitle
                    title="Settings"
                />
                <CustomButton type="secondary" onPressListner={this.onPressListener}>LOGOUT</CustomButton>
                <Toast
                    ref="toast"
                    style={AppStyles.toastStyle}
                    textStyle={{ color: 'white', paddingLeft: 20, paddingRight: 20 }}
                />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start'
    },
    titleStyle: {
        fontFamily: AppStyles.fonts.FONT_REGULAR,
        color: AppStyles.color.COLOR_SENCONDARY_BUTTON,
        fontSize: 14,
    },
    headerStyle: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: AppStyles.color.COLOR_BORDER,
        borderBottomWidth: 1
    },
    offerContainer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    offerImgStyle: {
        height: 150,
        width: 150
    }
});

const mapStateToProps = (state) => ({
    loading: selectors.getLoadingStatus(state),
    loginAccess: selectors.getLoginAccess(state)
});

const mapDispatchToProps = (dispatch) => ({
    logoutAction: (payload) => actions.logoutAction(dispatch, payload),
})

export default connect(mapStateToProps, mapDispatchToProps)(SettingScreen);