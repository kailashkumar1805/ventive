import { Actions } from "../actions/AcitonConstants";
import { initialState } from "./InitialState";

export const AccountReducer = (state = initialState.account, action) => {
    switch(action.type) {
        case Actions.LOGIN_ACCESS: {
            const login_access = action.login_access;
            return {
                ...state,
                login_access,
            }
        }
        case Actions.SIGNUP_ACCESS: {
            const users = action.payload;
            return {
                ...state,
                users
            }
        }
        default:
            return {
                ...state
            }
    }
}