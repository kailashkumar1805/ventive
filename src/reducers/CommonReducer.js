import { Actions } from "../actions/AcitonConstants";
import { initialState } from "./InitialState";

export const CommonReducer = (state = initialState.common, action) => {
    switch(action.type) {
        case Actions.API_PENDING:
            return {
                ...state,
                loading: true
            }
        case Actions.API_SUCCESS:
            return {
                ...state,
                loading: false
            }
        default:
            return {
                ...state
            }
    }
}