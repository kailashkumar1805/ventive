
import { combineReducers } from "redux";
import { CommonReducer } from "./CommonReducer";
import { AccountReducer } from "./AccountReducer";

export const rootReducer = combineReducers({
    common: CommonReducer,
    account: AccountReducer
});