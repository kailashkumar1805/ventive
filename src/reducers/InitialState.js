/**
 * Initial state for app
 */
export const initialState = {
    account: {
        users: []
    },
    common: {
        loading: false
    }
}