import { AsyncStorage } from 'react-native';

/**
 * get specific key value of array
 */
export const getKeyArray = (list, key) => {
    const arr = [];
    list.forEach(item => {
        arr.push(item[key])
    });
    return arr;
};


/**
 * get bag async storage count
 */
export const getBagCount = async () => {
    const bag = await AsyncStorage.getItem('onlineStoreBag', (error, result) => {
        if (result !== null) {
            return JSON.parse(result);
        }
    });
    console.log("bag", bag)
    return bag;
}


/**
 * get login token
 */
export const checkLoginToken = async () => {
    await AsyncStorage.getItem('onlineStoreLogin', (error, result) => {
        if (result !== null) {
            return true
        } else {
            return false
        }
    });
}

/**
 * Email validation
 */
export const validateEmail = (email) => {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(email) == false) 
    {
        return false;
    }
    return true;
}