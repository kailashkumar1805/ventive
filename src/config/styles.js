import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
/*
 * Provides universal color configs used in the app.
 * Provides universal fonts used in the app.
 */
const AppStyles = {
    color: {
        // COLOR_PRIMARY_BUTTON: '#FE952A', // orange
        COLOR_SENCONDARY_LIGHT: '#7E8D9A', // gray
        COLOR_PRIMARY_BUTTON: '#FF6600', // orange FF6600
        COLOR_SENCONDARY_BUTTON: '#262E3A', // gray 262E3A
        COLOR_TEXT: '#717D88',
        COLOR_BORDER: '#EDEDED',
        COLOR_RED_BORDER: '#FAB3C4',
        COLOR_WHITE: '#FFFFFF',
        COLOR_BLACK: '#000000',
        COLOR_GREY: 'grey',
        COLOR_LIGHT_GRAY: '#C0C0C0',
        COLOR_GREEN: 'green',
        COLOR_PLACEHOLDER: '#BEC2C3',
        COLOR_GREY_WHITE: '#fafafa',
        COLOR_DARK_SEPERATOR: '#d4d4d4',
        COLOR_BLACK_TRANSP: 'rgba(0, 0, 0, 0.7)',
        COLOR_GREY_TRANSP: 'rgba(67, 85, 85, 0.7)',
        COLOR_AC1: "#BCDCFF",
        COLOR_AC2: "#FFE5CC",
        COLOR_AC3: "#FFF0E0",
        COLOR_AC4: "#FFFCF7",
        COLOR_AC5: "#EFFCF6",
    },
    fonts: {
        FONT_REGULAR: 'Roboto-Regular',
        FONT_MEDIUM: 'Roboto-Medium'
    },
    inputStyle: {
        flex: 1,
        padding: 16,
        borderWidth: 1,
        borderColor: "#EDEDED",
        backgroundColor: "#FAFAFA",
        borderRadius: 25,
        alignSelf: 'stretch',
        color: "#262E3A",
        textAlign: 'left'
    },
    errorInputStyle: {
        borderColor: 'red'
    },
    bottomIconStyle: {
        height: 20,
        width: 20,
    },
    
    greenStyle: {
        color: "#7FB662"
    },

    yellowStyle: {
        color: "#F1C069",
    },

    redStyle: {
        color: "#F77E79",
    },
    toastStyle: {
        backgroundColor: '#262E3A',
        borderRadius: 50
    },
    activityStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        top: height / 2
    },
    selectTabIcon: {
        borderTopColor: '#FF6600',
        borderTopWidth: 3,
        padding: 7,
        paddingLeft: 14,
        paddingRight: 14
    }
};
export default AppStyles;
