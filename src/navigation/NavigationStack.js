import {
    createStackNavigator,
    createAppContainer,
    createBottomTabNavigator
} from 'react-navigation';
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';
import HomeScreen from '../screens/HomeScreen';
import MoreScreen from '../screens/MoreScreen';
import SettingScreen from '../screens/SettingScreen';
import AppStyles from '../config/styles';

const tabsNames = {
    Home: HomeScreen,
    More: MoreScreen,
    Setting: SettingScreen,
}

const TabBar = createBottomTabNavigator(tabsNames, {
    tabBarOptions: {
        activeTintColor: AppStyles.color.COLOR_PRIMARY_BUTTON,
        inactiveTintColor: AppStyles.color.COLOR_SENCONDARY_BUTTON,
        style: {
            backgroundColor: '#ffffff',
            color: AppStyles.color.COLOR_SENCONDARY_BUTTON
        }
    },
    initialRouteName: 'Home',
});

const RootStack = createStackNavigator({
    SignIn: {
        screen: SignInScreen,
        navigationOptions: { header: null, gesturesEnabled: false }
    },
    SignUp: {
        screen: SignUpScreen,
        navigationOptions: { header: null, gesturesEnabled: false }
    },
    Home: {
        screen: TabBar,
    } 
},{
    initialRouteName: 'SignIn'
});

const Ventive = createAppContainer(RootStack);

export default Ventive;
