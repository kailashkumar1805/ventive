import { action as AccountAction } from "./AccountAction";

export const actions = {
    ...AccountAction
}