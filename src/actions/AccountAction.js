import { Actions } from "./AcitonConstants";
import { AsyncStorage } from 'react-native';

export const action = {
    loginAccess: async (dispatch, payload) => {
        dispatch({ type: Actions.API_PENDING });
        await AsyncStorage.getItem('ventiveUsers', (error, result) => {
            if (result !== null) {
                dispatch({ type: Actions.LOGIN_ACCESS, login_access: JSON.parse(result) });
            } else {
                dispatch({ type: Actions.LOGIN_ACCESS, login_access: undefined });
            }
        });
    },
    userLogin: async (dispatch, payload) => {
        dispatch({ type: Actions.API_PENDING });
        await AsyncStorage.getItem('ventiveUsers', (error, result) => {
            if (result !== null) {
                const users = JSON.parse(result);
                console.log("user list", users);
                users.forEach((item) => {
                    if (item.email === payload.email && item.password === payload.password) {
                        AsyncStorage.setItem('ventiveLoginAccess', JSON.stringify(item), () => {
                            dispatch({ type: Actions.LOGIN_ACCESS, login_access: item });
                        });
                    }
                });
            } else {
                dispatch({ type: Actions.LOGIN_ACCESS, login_access: undefined });
            }
        });
    },
    signUpAccess: async (dispatch, payload) => {
        const users = [];
        users.push(payload);
        dispatch({ type: Actions.API_PENDING });
        await AsyncStorage.setItem('ventiveUsers', JSON.stringify(users), () => {
            dispatch({ type: Actions.SIGNUP_ACCESS, payload: users });
            dispatch({ type: Actions.API_SUCCESS });
        });
    },
    logoutAction: (dispatch) => {
        dispatch({ type: Actions.API_PENDING });
        dispatch({ type: Actions.LOGIN_ACCESS, login_access: undefined });

    }
}