import { getUserListSelector, getLoginAccess } from './AccountSelector'; 
import { getLoadingStatus } from './CommonSelector';

export const selectors = {
    getUserListSelector,
    getLoadingStatus,
    getLoginAccess
}