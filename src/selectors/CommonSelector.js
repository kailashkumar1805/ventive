/** Common selectors for app */

import { createSelector } from "reselect";

const loading = (state) => state.common.loading;

export const getLoadingStatus = createSelector(
    [loading],
    (loadingObj) => loadingObj
);
