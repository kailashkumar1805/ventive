/**
 * Product store selectors
 */

import { createSelector } from "reselect";

const users = (state) => state.account.users;
const loginAccess = (state) => state.account.login_access;

/**
 * get user list
 */
export const getUserListSelector = createSelector(
    [users],
    (usersObj) => usersObj
);

/**
 * get login access
 */
export const getLoginAccess = createSelector(
    [loginAccess],
    loginObj => loginObj
)