import { createStore, applyMiddleware, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import { multiClientMiddleware } from "redux-axios-middleware";
import { AsyncStorage } from "react-native";
import logger from 'redux-logger';
import thunkMiddleware from "redux-thunk";
import { name as appName } from "../../app.json";
import { rootReducer } from "../reducers/index.js";

const persistConfig = {
  key: "root",
  blacklist: [],
  whitelist: ["auth"],
  keyPrefix: appName,
  storage: AsyncStorage
};
const middlewares = [
  thunkMiddleware,
  logger
];
const persistedReducer = persistReducer(persistConfig, rootReducer);
export default () => {
  let store = createStore(persistedReducer,
    applyMiddleware(...middlewares)
  );
  let persistor = persistStore(store);
  return { store, persistor };
};