import React, { Component } from "react";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import { PersistGate } from "redux-persist/integration/react";
import Navigator from './navigation';

export default class Root extends Component {
  constructor(props) {
    super(props);
    const { persistor, store } = configureStore();
    this.persistor = persistor;
    this.store = store;
  }
  render() {
    return (
        <Provider store={this.store}>
          <PersistGate persistor={this.persistor}>
            <Navigator/>
          </PersistGate>
         </Provider>
    );
  }
}